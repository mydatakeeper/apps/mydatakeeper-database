
# Mydatakeeper Database

## Presentation

This project aims to store requests information and provide query interface for Mydatakeeper.

A XML Dbus interface description can be found in dbus-desc/adaptor.

## Usage

### Prerequisites

You will need:

 * A modern C/C++ compiler
 * CMake 3.1+ installed

### Building The Project

```shell_session
$ git clone https://gitlab.com/mydatakeeper/apps/mydatakeeper-database.git
$ cd mydatakeeper-database
$ mkdir build
$ cd build
$ cmake ..
$ make -j8
```

### Installing the project

```shell_session
# make install
```

## Project Structure

There are five folders: `src`, `include`, `dbus-desc`, `resources` and `scripts`. Each folder serves a self-explanatory purpose.

Source files are in `src`. Header files are in `include`. Dbus description files in XML format are in `dbus-desc`. Mydatakeeper application resources (a manifest, a logo and a webinterface) are in `resources`. Runtime scripts are available in `scripts`.

## Contributing

**Merge Requests are WELCOME!** Please submit any fixes or improvements, and I promise to review it as soon as I can at the project URL:

 * [Project Gitlab Home](https://gitlab.com/mydatakeeper/apps/mydatakeeper-database)
 * [Submit Issues](https://gitlab.com/mydatakeeper/apps/mydatakeeper-database/-/issues)
 * [Merge Requests](https://gitlab.com/mydatakeeper/apps/mydatakeeper-database/-/merge_requests)

## License

&copy; 2019-2020 Mydatakeeper S.A.S.

Open sourced under GPLv3 license. See attached LICENSE file.
