#ifndef DNSMASQ_PROXY_H_INCLUDED
#define DNSMASQ_PROXY_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "proxy/uk.org.thekelleys.dnsmasq.h"

class DnsmasqProxy
: public uk::org::thekelleys::dnsmasq_proxy,
  public DBusProxy
{
public:
    DnsmasqProxy(DBus::Connection &connection, const string& path, const string& name)
    : DBusProxy(connection, path, name) {}
    virtual ~DnsmasqProxy() {}

    virtual void DhcpLeaseAdded(const std::string& ipaddr, const std::string& hwaddr, const std::map< std::string, std::string >& metadata)
    {
        if (nullptr != onDhcpLeaseAdded)
            onDhcpLeaseAdded(ipaddr, hwaddr, metadata);
    }

    virtual void DhcpLeaseDeleted(const std::string& ipaddr, const std::string& hwaddr, const std::map< std::string, std::string >& metadata)
    {
        if (nullptr != onDhcpLeaseDeleted)
            onDhcpLeaseDeleted(ipaddr, hwaddr, metadata);
    }

    virtual void DhcpLeaseUpdated(const std::string& ipaddr, const std::string& hwaddr, const std::map< std::string, std::string >& metadata)
    {
        if (nullptr != onDhcpLeaseUpdated)
            onDhcpLeaseUpdated(ipaddr, hwaddr, metadata);
    }

    virtual void ResolveDomain(const std::string& source, const uint16_t& id, const bool& query, const uint8_t& operation_code, const bool& autoritative_answer, const bool& truncation, const bool& recursion_desired, const bool& recursion_available, const bool& authenticated_data, const bool& checking_disabled, const uint8_t& response_code, const std::vector< ::DBus::Struct< std::string, std::string, std::string > >& questions, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& answers, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& authorities, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& additionals, const std::vector< ::DBus::Struct< uint16_t, uint16_t, uint16_t, std::vector< ::DBus::Struct< uint16_t, std::vector< uint8_t > > > > >& pseudoheaders)
    {
        if (nullptr != onResolveDomain)
            onResolveDomain(
                source,
                id,
                query,
                operation_code,
                autoritative_answer,
                truncation,
                recursion_desired,
                recursion_available,
                authenticated_data,
                checking_disabled,
                response_code,
                questions,
                answers,
                authorities,
                additionals,
                pseudoheaders
            );
    }

    typedef void (DhcpSignal_t)(const std::string& ipaddr, const std::string& hwaddr, const std::map< std::string, std::string >& metadata);
    typedef void (ResolveSignal_t)(const std::string& source, const uint16_t& id, const bool& query, const uint8_t& operation_code, const bool& autoritative_answer, const bool& truncation, const bool& recursion_desired, const bool& recursion_available, const bool& authenticated_data, const bool& checking_disabled, const uint8_t& response_code, const std::vector< ::DBus::Struct< std::string, std::string, std::string > >& questions, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& answers, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& authorities, const std::vector< ::DBus::Struct< std::string, std::string, std::string, uint32_t, ::DBus::Variant > >& additionals, const std::vector< ::DBus::Struct< uint16_t, uint16_t, uint16_t, std::vector< ::DBus::Struct< uint16_t, std::vector< uint8_t > > > > >& pseudoheaders);



    function<DhcpSignal_t> onDhcpLeaseAdded = nullptr;
    function<DhcpSignal_t> onDhcpLeaseDeleted = nullptr;
    function<DhcpSignal_t> onDhcpLeaseUpdated = nullptr;
    function<ResolveSignal_t> onResolveDomain = nullptr;
};

#endif /* DNSMASQ_PROXY_H_INCLUDED */
