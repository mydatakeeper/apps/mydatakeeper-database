#ifndef DATABASE_ADAPTOR_H_INCLUDED
#define DATABASE_ADAPTOR_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "adaptor/fr.mydatakeeper.app.database.h"
#include "database.h"

extern const string DatabasePath;

using entry = map<string, string>;

class DatabaseAdaptor
: public fr::mydatakeeper::app::database_adaptor,
  public DBusAdaptor
{
public:
    DatabaseAdaptor(DBus::Connection &connection, Database& db)
    : DBusAdaptor(connection, DatabasePath), db(db) {}
    virtual ~DatabaseAdaptor() {}

    virtual std::vector<entry> query(const std::map< std::string, std::string >& filters, const std::string& group_by);

private:
    Database& db;
};

#endif /* DATABASE_ADAPTOR_H_INCLUDED */
