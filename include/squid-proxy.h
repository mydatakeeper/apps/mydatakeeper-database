#ifndef SQUID_PROXY_H_INCLUDED
#define SQUID_PROXY_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "proxy/org.squid_cache.proxy.h"

class SquidProxy
: public org::squid_cache::proxy_proxy,
  public DBusProxy
{
public:
    SquidProxy(DBus::Connection &connection, const string& path, const string& name)
    : DBusProxy(connection, path, name) {}
    virtual ~SquidProxy() {}

    virtual void RequestUrl(const string& src, const string& domain, const string& url, const map<string, string>& metadata)
    {
        if (nullptr != onRequestUrl)
            onRequestUrl(src, domain, url, metadata);
    }

    typedef void (ResolveSignal_t)(const string&, const string&, const string&, const map<string, string>&);

    function<ResolveSignal_t> onRequestUrl = nullptr;
};

#endif /* SQUID_PROXY_H_INCLUDED */
