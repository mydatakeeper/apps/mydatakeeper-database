#ifndef DATABASE_H_INCLUDED
#define DATABASE_H_INCLUDED

#include <string>
#include <vector>
#include <map>

struct Database
{
    struct Value
    {
        double d;
        int i;
        std::string str;

        enum {
            DOUBLE_TYPE,
            INT_TYPE,
            TEXT_TYPE,
            NULL_TYPE,
        } type;

        Value(double d) : d(d), type(DOUBLE_TYPE) {}
        Value(int i) : i(i), type(INT_TYPE) {}
        Value(const std::string& str) : str(str), type(TEXT_TYPE) {}
        Value() : type(NULL_TYPE) {}
    };

    ~Database();

    bool open(const std::string &filename);
    bool exec(
        const std::string& sql,
        const std::map<std::string, Value>& args,
        std::vector<std::vector<Value>> *results = nullptr);

private:
    void *db;
};

#endif /* DATABASE_H_INCLUDED */
