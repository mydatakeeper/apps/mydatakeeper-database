#include "database.h"

#include <sqlite3.h>

#include <iostream>

using namespace std;

namespace {
bool bind(sqlite3_stmt *stmt, const string& key, const Database::Value& value)
{
    int idx = sqlite3_bind_parameter_index(stmt, key.c_str());
    if (!idx)
    {
        cerr << "Can't bind parameter '" << key << "' to statement: undefined parameter" << endl;
        return false;
    }

    int res = 0;
    switch (value.type)
    {
        case Database::Value::DOUBLE_TYPE:
            res = sqlite3_bind_double(stmt, idx, value.d);
            break;
        case Database::Value::INT_TYPE:
            res = sqlite3_bind_int(stmt, idx, value.i);
            break;
        case Database::Value::NULL_TYPE:
            res = sqlite3_bind_null(stmt, idx);
            break;
        case Database::Value::TEXT_TYPE:
            res = sqlite3_bind_text(stmt, idx, value.str.c_str(), value.str.size(), nullptr);
            break;
    }

    if (res)
    {
        cerr << "Can't bind parameter '" << key << "' to statement: " << sqlite3_errstr(res) << endl;
        return false;
    }

    return true;
}

#ifdef DEBUG
std::ostream& operator<< (std::ostream& out, const Database::Value& value)
{
    switch (value.type)
    {
        case Database::Value::DOUBLE_TYPE:
            return out << value.d;
        case Database::Value::INT_TYPE:
            return out << value.i;
        case Database::Value::NULL_TYPE:
            return out << nullptr;
        case Database::Value::TEXT_TYPE:
            return out << value.str;
    }

    return out;
}
#endif /* DEBUG */

} /* end of namespace */

Database::~Database()
{
    if (db) {
        sqlite3_close((sqlite3*)db);
        db = nullptr;
    }
}

bool Database::open(const std::string &filename)
{
    if (sqlite3_open(filename.c_str(), (sqlite3**)&db)) {
        cerr << "Can't open database: " << sqlite3_errmsg((sqlite3*)db) << endl;
        return false;
    }

    clog << "Opened database successfully" << endl;
    return true;
}

bool Database::exec(const string& sql, const map<string, Value>& args, vector<vector<Value>> *results)
{
    sqlite3_stmt *stmt;

    if (!db) return false;

#ifdef DEBUG
    clog << sql << endl;
    for (auto& it : args)
        clog << '\t' << it.first << '=' << it.second << endl;
#endif /* DEBUG */

    if (sqlite3_prepare_v2((sqlite3*)db, sql.c_str(), -1, &stmt, NULL)) {
        cerr << "Can't prepare statement: " << sqlite3_errmsg((sqlite3*)db) << endl;
        return false;
    }

    for (auto& it : args) {
        if (!bind(stmt, it.first, it.second)) {
            sqlite3_finalize(stmt);
            return false;
        }
    }

    if (results)
        results->clear();

    while (true)
    {
        int res = sqlite3_step(stmt);
        if (res == SQLITE_DONE)
            break;

        if (res == SQLITE_ROW) {
            if (!results)
                continue;

            vector<Value> row;
            int cols = sqlite3_column_count(stmt);
            for (int i = 0; i < cols; ++i)
            {
                switch (sqlite3_column_type(stmt, i))
                {
                    case SQLITE_INTEGER:
                        row.emplace_back(sqlite3_column_int(stmt, i));
                        break;
                    case SQLITE_FLOAT:
                        row.emplace_back(sqlite3_column_double(stmt, i));
                        break;
                    case SQLITE_TEXT:
                        row.emplace_back(string((const char*)sqlite3_column_text(stmt, i)));
                        break;
                    case SQLITE_NULL:
                        row.emplace_back();
                        break;
                    case SQLITE_BLOB:
                    default:
                        cerr << "Data type not supported" << endl;
                        row.emplace_back();
                        break;
                }
            }

            results->emplace_back(move(row));
            continue;
        }

        cerr << "Can't step statement: " << sqlite3_errmsg((sqlite3*)db) << endl;
        sqlite3_finalize(stmt);
        return false;
    }

    sqlite3_finalize(stmt);
    return true;
}
