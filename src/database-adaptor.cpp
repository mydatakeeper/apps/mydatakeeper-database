#include "database-adaptor.h"

#include <set>

using namespace std;

const string DatabasePath("/fr/mydatakeeper/app/database");

namespace {
const string& get(const map<string, string>& m, const string& key, const string& defval = {})
{
    auto it = m.find(key);
    if (it == m.end())
        return defval;
    return it->second;
}
} /* End of anonmous namespace */

vector<entry> DatabaseAdaptor::query(
    const map<string, string>& filters,
    const string &group_by)
{
    string type = get(filters, "type", "");
    string domain = get(filters, "domain", "");
    string blocked = get(filters, "blocked", "");
    string hwaddr = get(filters, "hwaddr", "");
    string hostname = get(filters, "hostname", "");
    string company = get(filters, "company", "");
    string start = get(filters, "start", "");
    string end = get(filters, "end", "");
    string offset = get(filters, "offset", "0");
    string limit = get(filters, "limit", "100");

    std::map<std::string, Database::Value> args;
    if (!type.empty()) { args[":type"] = type; }
    if (!domain.empty()) { args[":domain"] = domain; }
    if (!blocked.empty() && blocked != "NULL") { args[":blocked"] = blocked; }
    if (!hwaddr.empty()) { args[":hwaddr"] = hwaddr; }
    if (!hostname.empty()) { args[":hostname"] = hostname; }
    if (!company.empty()) { args[":company"] = company; }
    if (!start.empty()) { args[":start"] = start; }
    if (!end.empty()) { args[":end"] = end; }
    if (!offset.empty()) { args[":offset"] = offset; }
    if (!limit.empty()) { args[":limit"] = limit; }

    const set<string> valid_group = {
        "domain", "type", "blocked", "hwaddr", "hostname", "company"
    };
    bool group = valid_group.find(group_by) != valid_group.end();

    const string sql = "WITH query as ("
        "SELECT "
            "'dns' as type, "
            "dns.device, "
            "dns.created, "
            "dns.domain, "
            "dns_meta.value as blocked "
        "FROM "
            "dns "
        "LEFT JOIN dns_meta "
            "ON dns.id == dns_meta.id "
            "AND dns_meta.name == 'BLOCKED' "
        "UNION "
        "SELECT "
            "'http' as type, "
            "http.device, "
            "http.created, "
            "http.domain, "
            "http_meta.value as blocked "
        "FROM "
            "http "
        "LEFT JOIN http_meta "
            "ON http.id == http_meta.id "
            "AND http_meta.name == 'BLOCKED' "
    "), full_query as ( "
        "SELECT "
            "query.type, "
            "query.domain, "
            "query.blocked, "
            "device.hwaddr, "
            "device_meta.value as hostname, "
            "company.name as company, "
            "query.created "
        "FROM query "
        "JOIN device "
            "ON device.id == query.device "
        "LEFT JOIN device_meta "
            "ON device_meta.id == device.id "
            "AND device_meta.name == 'SUPPLIED_HOSTNAME' "
        "LEFT JOIN company "
            "ON company.domain == query.domain "
    ") "
    + (group
        ? "SELECT " +
            group_by + ", " +
            (group_by == "hostname" ? "hwaddr, " : "") +
            "COUNT(*) as count " +
            (blocked.empty() && group_by != "blocked"
                ? ", COUNT(blocked) as blocked_count "
                : "" )
        : "SELECT * "
    ) +
    "FROM full_query "
        "WHERE 1==1 "
        + (type.empty() ? "" : "AND type LIKE :type ")
        + (domain.empty() ? "" : "AND domain LIKE :domain ")
        + (blocked.empty() ? ""
            : (blocked == "NULL"
                ? "AND blocked IS NULL "
                : "AND blocked LIKE :blocked "))
        + (hwaddr.empty() ? "" : "AND hwaddr LIKE :hwaddr ")
        + (hostname.empty() ? "" : "AND hostname LIKE :hostname ")
        + (company.empty() ? "" : "AND company LIKE :company ")
        + (start.empty() ? "" : "AND created > DATE(:start) ")
        + (end.empty() ? "" : "AND created <= DATE(:end) ")
    + (group
        ? "GROUP BY " + group_by + " ORDER BY " +
            (blocked.empty() && group_by != "blocked"
                ? "blocked_count"
                : "count") +
            " DESC "
        : ""
    ) +
    "LIMIT :limit "
    "OFFSET :offset";

    vector<vector<Database::Value>> values;
    if (!db.exec(sql, args, &values))
        throw DBus::ErrorFailed("Unable to access database");

    vector<entry> result;
    for (auto& it : values)
    {
        result.emplace_back();
        entry& row = result.back();
        if (group) {
            size_t i = 0;
            if (it[i].type != Database::Value::NULL_TYPE) {
                assert(it[i].type == Database::Value::TEXT_TYPE);
                row[group_by] = it[i].str;
            }
            i++;

            if (group_by == "hostname") {
                assert(it[i].type == Database::Value::TEXT_TYPE);
                row["hwaddr"] = it[i].str;
                i++;
            }

            assert(it[i].type == Database::Value::INT_TYPE);
            row["count"] = to_string(it[i].i);
            i++;

            if (blocked.empty() && group_by != "blocked") {
                assert(it[i].type == Database::Value::INT_TYPE);
                row["blocked_count"] = to_string(it[i].i);
            }
        } else {
            assert(it[0].type == Database::Value::TEXT_TYPE);
            row["type"] = it[0].str;
            assert(it[1].type == Database::Value::TEXT_TYPE);
            row["domain"] = it[1].str;
            if (it[2].type != Database::Value::NULL_TYPE) {
                assert(it[2].type == Database::Value::TEXT_TYPE);
                row["blocked"] = it[2].str;
            }
            assert(it[3].type == Database::Value::TEXT_TYPE);
            row["hwaddr"] = it[3].str;
            if (it[4].type != Database::Value::NULL_TYPE) {
                assert(it[4].type == Database::Value::TEXT_TYPE);
                row["hostname"] = it[4].str;
            }
            if (it[5].type != Database::Value::NULL_TYPE) {
                assert(it[5].type == Database::Value::TEXT_TYPE);
                row["company"] = it[5].str;
            }
            assert(it[6].type == Database::Value::TEXT_TYPE);
            row["created"] = it[6].str;
        }
    }

    return result;
}
