#include <csignal>
#include <fstream>
#include <iostream>
#include <set>
#include <limits.h>
#include <unistd.h>

#include "dnsmasq-proxy.h"
#include "squid-proxy.h"
#include "database-adaptor.h"
#include "database.h"

#include <mydatakeeper/arguments.h>

using namespace std;

DBus::BusDispatcher dispatcher;

using device_t = DBus::Struct<string, string, map<string, string>>;

struct device {
    string hwaddr;
    string ipaddr;
    map<string, string> metadata;

    device(const string& hwaddr, const string& ipaddr, const map<string,string>& metadata)
        : hwaddr(hwaddr)
        , ipaddr(ipaddr)
        , metadata(metadata)
    {}

    device(const device_t& device)
        : hwaddr(device._1)
        , ipaddr(device._2)
        , metadata(device._3)
    {}

    bool operator < (const device& rhs) const
    {
        return ipaddr.compare(rhs.ipaddr) < 0;
    }
};

set<device> arptable;
set<device> devices;
string db_file;
Database db;


#define XSTR(s) STR(s)
#define STR(s) #s

#define ARP_CACHE       "/proc/net/arp"
#define ARP_STRING_LEN  1023
#define ARP_BUFFER_LEN  (ARP_STRING_LEN + 1)

/* Format for fscanf() to read the 1st, 4th, and 6th space-delimited fields */
#define ARP_LINE_FORMAT "%" XSTR(ARP_STRING_LEN) "s %*s %*s " \
                        "%" XSTR(ARP_STRING_LEN) "s %*s " \
                        "%" XSTR(ARP_STRING_LEN) "s"

void update_arptable()
{
    FILE *arpCache = fopen(ARP_CACHE, "r");
    if (!arpCache)
    {
        cerr << "Failed to open file \"" ARP_CACHE "\"";
        return;
    }

    /* Ignore the first line, which contains the header */
    char header[ARP_BUFFER_LEN];
    if (!fgets(header, sizeof(header), arpCache))
        return;

    arptable.clear();

    char ipaddr[ARP_BUFFER_LEN];
    char hwaddr[ARP_BUFFER_LEN];
    char device[ARP_BUFFER_LEN];
    while (3 == fscanf(arpCache, ARP_LINE_FORMAT, ipaddr, hwaddr, device))
        arptable.emplace(string(hwaddr), string(ipaddr), map<string, string>());

    fclose(arpCache);
}

void add_device(
    const string& ipaddr,
    const string& hwaddr,
    const map<string, string>& metadata)
{
    static const string sql =
        "INSERT OR IGNORE INTO device(hwaddr, ipaddr) "
        "VALUES (:hwaddr, :ipaddr)";
    const map<string, Database::Value> values = {
        {":hwaddr", hwaddr},
        {":ipaddr", ipaddr},
    };

    if (!db.exec("BEGIN TRANSACTION", {})) goto cleanup;
    if (!db.exec(sql, values)) goto cleanup;

    if (metadata.size()) {
        stringstream sql;
        sql << "WITH lease AS ( "
            <<  "SELECT id "
            <<  "FROM device "
            <<  "WHERE hwaddr=:hwaddr AND ipaddr=:ipaddr "
            <<  "ORDER BY created DESC LIMIT 1"
            << ") "
            << "INSERT OR REPLACE INTO device_meta(id, name, value) "
            << "VALUES ";
        map<string, Database::Value> values = {
            {":hwaddr", hwaddr},
            {":ipaddr", ipaddr},
        };
        size_t count = 0;
        for (auto& it : metadata) {
            stringstream key, value;
            key << ":key" << count;
            value << ":value" << count;
            if (count)
                sql << ", ";
            sql << "( "
                <<  "(SELECT id FROM lease), "
                <<  key.str() << ", "
                <<  value.str()
                << ")";
            string prefix("DNSMASQ_");
            string k(it.first);
            if (!k.compare(0, prefix.size(), prefix))
                k.erase(0, prefix.size());
            values[key.str()] = k;
            values[value.str()] = it.second;
            count++;
        }
        if (!db.exec(sql.str(), values))
            goto cleanup;
    }

    if (!db.exec("COMMIT TRANSACTION", {})) goto cleanup;
    devices.emplace(hwaddr, ipaddr, metadata);
    clog << "DHCP device added: " << hwaddr << ',' << ipaddr << endl;
    return;

cleanup:
    db.exec("ROLLBACK", {});
    cerr << "DHCP device not added: " << hwaddr << ',' << ipaddr << endl;
}

void delete_device(
    const string& ipaddr,
    const string& hwaddr,
    const map<string, string>& metadata)
{
    static const string sql =
        "INSERT OR REPLACE INTO device_meta(id, name, value)"
        " VALUES ("
            "("
                "SELECT id "
                "FROM device "
                "WHERE hwaddr=:hwaddr AND ipaddr=:ipaddr "
                "ORDER BY created DESC LIMIT 1"
            "), "
            "'LEASE_EXPIRES', "
            "strftime('%s', CURRENT_TIMESTAMP)"
        ")";
    const map<string, Database::Value> values = {
        {":hwaddr", hwaddr},
        {":ipaddr", ipaddr},
    };
    if (!db.exec(sql, values))
        goto cleanup;

    devices.erase(device(hwaddr, ipaddr, metadata));
    clog << "DHCP device deleted: " << hwaddr << ',' << ipaddr << endl;
    return;

cleanup:
    db.exec("ROLLBACK", {});
    cerr << "DHCP device not deleted: " << hwaddr << ',' << ipaddr << endl;
}

void update_device(
    const string& ipaddr,
    const string& hwaddr,
    const map<string, string>& metadata)
{
    if (!db.exec("BEGIN TRANSACTION", {})) goto cleanup;

    if (metadata.size()) {
        stringstream sql;
        sql << "WITH lease AS ( "
            <<  "SELECT id "
            <<  "FROM device "
            <<  "WHERE hwaddr=:hwaddr AND ipaddr=:ipaddr "
            <<  "ORDER BY created DESC LIMIT 1"
            << ") "
            << "INSERT OR REPLACE INTO device_meta(id, name, value) "
            << "VALUES ";
        map<string, Database::Value> values = {
            {":hwaddr", hwaddr},
            {":ipaddr", ipaddr},
        };
        size_t count = 0;
        for (auto& it : metadata) {
            stringstream key, value;
            key << ":key" << count;
            value << ":value" << count;
            if (count)
                sql << ", ";
            sql << "( "
                <<  "(SELECT id FROM lease), "
                <<  key.str() << ", "
                <<  value.str()
                << ")";
            string prefix("DNSMASQ_");
            string k(it.first);
            if (!k.compare(0, prefix.size(), prefix))
                k.erase(0, prefix.size());
            values[key.str()] = k;
            values[value.str()] = it.second;
            count++;
        }
        if (!db.exec(sql.str(), values))
            goto cleanup;
    }

    if (!db.exec("COMMIT TRANSACTION", {})) goto cleanup;
    devices.emplace(hwaddr, ipaddr, metadata);
    clog << "DHCP device updated: " << hwaddr << ',' << ipaddr << endl;
    return;

cleanup:
    db.exec("ROLLBACK", {});
    cerr << "DHCP device not updated: " << hwaddr << ',' << ipaddr << endl;
}

void initialize_devices(auto& dnsmasq)
{
    devices.clear();
    auto tmp = dnsmasq.GetDhcpLeases();
    clog << "Retrieving " << tmp.size() << " existing device(s)" << endl;
    for (auto& it : tmp)
        add_device(it._1, it._2, it._3);
}

void add_partial_device(const string& ipaddr)
{
    // When we don't have a mac address associated to an ip,
    // we still record the activity under a generic mac address
    string hwaddr = "00:00:00:00:00:00";

    auto device = devices.find({{}, ipaddr, {}});
    if (device != devices.end())
        return;

    update_arptable();

    auto arpentry = arptable.find({{}, ipaddr, {}});
    if (arpentry != arptable.end())
        hwaddr = arpentry->hwaddr;

    map<string, string> metadata;
    if (ipaddr == "127.0.0.1") {
        char hostname[HOST_NAME_MAX+1];
        if (gethostname(hostname, HOST_NAME_MAX) == 0) {
            metadata["SUPPLIED_HOSTNAME"] = string(hostname);
        }
    }

    add_device(ipaddr, hwaddr, metadata);
}

#define EDNS0_OPTION_BLOCKED 65075

void insert_dns_record(
    const string& ipaddr,
    const uint16_t& /*id*/,
    const bool& /*query*/,
    const uint8_t& /*operation_code*/,
    const bool& /*autoritative_answer*/,
    const bool& /*truncation*/,
    const bool& /*recursion_desired*/,
    const bool& /*recursion_available*/,
    const bool& /*authenticated_data*/,
    const bool& /*checking_disabled*/,
    const uint8_t& /*response_code*/,
    const vector< DBus::Struct< string, string, string > >& questions,
    const vector< DBus::Struct< string, string, string, uint32_t, DBus::Variant > >& answers,
    const vector< DBus::Struct< string, string, string, uint32_t, DBus::Variant > >& /*authorities*/,
    const vector< DBus::Struct< string, string, string, uint32_t, DBus::Variant > >& /*additionals*/,
    const vector< DBus::Struct< uint16_t, uint16_t, uint16_t, vector< DBus::Struct< uint16_t, vector< uint8_t > > > > >& pseudoheaders)
{
    add_partial_device(ipaddr);

    set<string> blocked;
    if (!db.exec("BEGIN TRANSACTION", {})) goto cleanup;

    for (auto& pseudoheader : pseudoheaders) {
        for (auto& option : pseudoheader._4) {
            if (option._1 == EDNS0_OPTION_BLOCKED) {
                string domain;
                stringstream sstr(string(option._2.begin(), option._2.end()));
                while(std::getline(sstr, domain, ',')) {
                    blocked.insert(domain);
                }                
            }
        }
    }

    for (auto& question : questions) {
        if (question._3 != "IN" && question._3 != "ANY")
            continue;
        bool is_blocked = blocked.find(question._1) != blocked.end();

        clog << "Received one query for " << question._1 << endl;
        static const string sql =
            "INSERT INTO dns(device, domain) "
            "VALUES ( "
             "("
              "SELECT id "
              "FROM device "
              "WHERE ipaddr=:ipaddr "
              "ORDER BY created DESC "
              "LIMIT 1"
             "), "
             ":domain"
            ")";
        const map<string, Database::Value> values = {
            {":ipaddr", ipaddr},
            {":domain", question._1},
        };
        if (!db.exec(sql, values))
            goto cleanup;

        if (answers.size() || is_blocked) {
            stringstream sql;
            sql << "INSERT OR REPLACE INTO dns_meta(id, name, value) "
                << "VALUES ";
            map<string, Database::Value> values = {};
            size_t count = 0;
            if (is_blocked) {
                sql << "( last_insert_rowid(), 'BLOCKED', 'true' )";
                count++;
            }
            for (auto& answer : answers) {
                if (answer._1 != question._1)
                    continue;
                if (answer._2 != "A" && answer._2 != "AAAA" && answer._2 != "CNAME")
                    continue;
                if (answer._3 != "IN")
                    continue;

                stringstream key, value;
                key << ":key" << count;
                value << ":value" << count;
                if (count)
                    sql << ", ";
                sql << "( "
                    <<  "last_insert_rowid(), "
                    <<  key.str() << ", "
                    <<  value.str()
                    << ")";
                values[key.str()] = answer._2;
                values[value.str()] = answer._5.operator string();
                count++;
            }
            if (count)
                if (!db.exec(sql.str().c_str(), values))
                    goto cleanup;
        }
    }

    if (!db.exec("COMMIT TRANSACTION", {})) goto cleanup;
    clog << questions.size() << " DNS records by " << ipaddr << " inserted" << endl;
    return;

cleanup:
    db.exec("ROLLBACK", {});
    clog << questions.size() << " DNS records by " << ipaddr << " not inserted" << endl;
}

void insert_http_record(
    const string& ipaddr,
    const string& domain,
    const string& url,
    const map<string, string>& metadata)
{
    add_partial_device(ipaddr);

    static const string sql =
        "INSERT INTO http(device, domain, url) "
        "VALUES ( "
         "("
          "SELECT id "
          "FROM device "
          "WHERE ipaddr=:ipaddr "
          "ORDER BY created DESC "
          "LIMIT 1"
         "), "
         ":domain, "
         ":url"
        ")";
    const map<string, Database::Value> values = {
        {":ipaddr", ipaddr},
        {":domain", domain},
        {":url", url},
    };

    if (!db.exec("BEGIN TRANSACTION", {})) goto cleanup;
    if (!db.exec(sql, values)) goto cleanup;

    if (metadata.size()) {
        stringstream sql;
        sql << "INSERT OR REPLACE INTO http_meta(id, name, value) "
            << "VALUES ";
        map<string, Database::Value> values = {};
        size_t count = 0;
        for (auto& it : metadata) {
            stringstream key, value;
            key << ":key" << count;
            value << ":value" << count;
            if (count)
                sql << ", ";
            sql << "( "
                <<  "last_insert_rowid(), "
                <<  key.str() << ", "
                <<  value.str()
                << ")";
            values[key.str()] = it.first;
            values[value.str()] = it.second;
            count++;
        }
        if (count)
            if (!db.exec(sql.str().c_str(), values))
                goto cleanup;
    }

    if (!db.exec("COMMIT TRANSACTION", {})) goto cleanup;
    clog << "HTTP record inserted: " << ipaddr << ',' << url << endl;
    return;

cleanup:
    db.exec("ROLLBACK", {});
    cerr << "HTTP record not inserted: " << ipaddr << ',' << url << endl;
}


void handler(int sig)
{
    clog << "Stopping DBus main loop (received " << strsignal(sig) << ')' << endl;
    dispatcher.leave();
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, database_bus_name, dnsmasq_bus_path, dnsmasq_bus_name, squid_bus_path, squid_bus_name, conf_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            database_bus_name, "database-bus-name", 0, mydatakeeper::required_argument, "fr.mydatakeeper.app.database", nullptr,
            "Bus name for database service"
        },
        {
            dnsmasq_bus_name, "dnsmasq-bus-name", 0, mydatakeeper::required_argument, "uk.org.thekelleys.dnsmasq", nullptr,
            "Bus name for dnsmasq service"
        },
        {
            dnsmasq_bus_path, "dnsmasq-bus-path", 0, mydatakeeper::required_argument, "/uk/org/thekelleys/dnsmasq", nullptr,
            "Bus path for dnsmasq service"
        },
        {
            squid_bus_name, "squid-bus-name", 0, mydatakeeper::required_argument, "org.squid_cache.proxy", nullptr,
            "Bus name for squid service"
        },
        {
            squid_bus_path, "squid-bus-path", 0, mydatakeeper::required_argument, "/org/squid_cache/proxy", nullptr,
            "Bus path for squid service"
        },
        {
            conf_dir, "conf-dir", 0, mydatakeeper::required_argument, "/var/lib/mydatakeeper-config/apps/fr.mydatakeeper.app.database", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    db_file = conf_dir + "/db.sqlite3";
    if (!db.open(db_file))
    {
        cerr << "Unable to open database file: " << db_file << endl;
        return 1;
    }

    clog << "Getting Dnsmasq " << dnsmasq_bus_path << " proxy" << endl;
    DnsmasqProxy dnsmasq(conn, dnsmasq_bus_path, dnsmasq_bus_name);

    clog << "Getting Squid " << squid_bus_path << " proxy" << endl;
    SquidProxy squid(conn, squid_bus_path, squid_bus_name);

    clog << "Getting Database adaptor" << endl;
    DatabaseAdaptor database(conn, db);

    clog << "Setting up DNS " << dnsmasq_bus_path << " listener" << endl;
    dnsmasq.onDhcpLeaseAdded = add_device;
    dnsmasq.onDhcpLeaseDeleted = delete_device;
    dnsmasq.onDhcpLeaseUpdated = update_device;
    dnsmasq.onResolveDomain = insert_dns_record;

    clog << "Setting up Proxy " << squid_bus_path << " listener" << endl;
    squid.onRequestUrl = insert_http_record;

    // Initialize device data
    initialize_devices(dnsmasq);
    update_arptable();

    clog << "Requesting DBus name" << endl;
    conn.request_name(database_bus_name.c_str());

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}