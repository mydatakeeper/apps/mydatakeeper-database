// Message handler
var handlers = [];

window.addEventListener("message", function (event) {
    if (event.origin !== window.location.origin)
        return;
    if (event.data.status !== 'success') {
        console.error(event.data.status + ': ' + event.data.error);
        return;
    }

    if (!handlers[event.data.id]) {
        console.error('error: missing callback for', event);
        return;
    }

    handlers[event.data.id](event.data.result);
    delete handlers[event.data.id];
}, false);

function methodCall(name, args, callback) {
    var id = Math.floor(Math.random() * Math.floor(65536));
    window.parent.postMessage({
        id: id,
        method: name,
        args: args,
    }, window.location.origin);
    handlers[id] = callback;
}

// Initialize chart
function setupChart(id, data, layout, config)
{
    var element = document.getElementById(id);
    if (!element) return;

    element.innerHTML = '';
    return new Plotly.newPlot(element, data, layout, config);
}

$(function(){
    methodCall('query', [{blocked: 'true'}, 'type'], function(raw) {
        var blocked_dns = raw.find(x => x.type == 'dns') || {count: 0};
        var blocked_http = raw.find(x => x.type == 'http') || {count: 0};
        var blocked = (+blocked_dns.count) + (+blocked_http.count);

        document.getElementById('blocked_count').innerHTML = blocked;
        document.getElementById('blocked_dns_count').innerHTML = +blocked_dns.count;
        document.getElementById('blocked_http_count').innerHTML = +blocked_http.count;
    });

    methodCall('query', [{limit: 10, blocked: 'true'}, 'hostname'], function(raw) {
        if (!raw.length) return;

        raw = raw.sort((x, y) => x.count - y.count)
        names = raw.map(
            x => x.hostname ? x.hostname + ' (' + x.hwaddr  + ')' : x.hwaddr
        );
        var chart = setupChart(
            'top_blocked_devices',
            [
                {
                    x: raw.map(x => x.count),
                    y: raw.map(x => x.hostname ? x.hostname : ''),
                    type: 'bar',
                    orientation: 'h',
                    marker: {
                        color: 'rgb(158,202,225)',
                        opacity: 0.4,
                        line: {
                            color: 'rgb(8,48,107)',
                            width: 1,
                        },
                    },
                },
            ], {
                margin: { l: 0, r: 0, t: 0, b: 0},
                height: 200,
                annotations: raw.map(x => ({
                    xanchor: 'left',
                    x: 0,
                    y: x.hostname ? x.hostname : '',
                    text: x.hostname ? x.hostname+' ('+x.hwaddr+')' : x.hwaddr,
                    showarrow: false,
                }))
            }, {
                responsive: true,
                displayModeBar: false,
            }
        );
    });

    methodCall('query', [{limit: 10, blocked: 'true'}, 'domain'], function(raw) {
        if (!raw.length) return;

        var chart = setupChart(
            'top_blocked_domains',
            [
                {
                    labels: raw.map(x => x.domain),
                    values: raw.map(x => x.count),
                    type: 'pie',
                    textinfo: 'none',
                    marker: {
                        opacity: 0,
                        line: {
                            width: 1,
                        },
                    },
                },
            ], {
                margin: { l: 0, r: 0, t: 0, b: 0},
                yanchor: 'center',
            }, {
                responsive: true,
                displayModeBar: false,
            }
        );
    });
});
