# Makefile
.SUFFIXES:

CXX:=$(CROSS_COMPILE)g++
CC:=$(CROSS_COMPILE)gcc
EXTRA_CXXFLAGS:=-std=c++17 -fconcepts -I./include $(shell pkg-config --cflags mydatakeeper sqlite3)
EXTRA_LDFLAGS:=$(shell pkg-config --libs mydatakeeper sqlite3)

TARGET=mydatakeeper-database
OBJ_FILES=\
	mydatakeeper-database.o \
	database-adaptor.o \
	database.o
INC_FILES=\
	dnsmasq-proxy.h \
	squid-proxy.h \
	database.h
GEN_FILES=\
	adaptor/fr.mydatakeeper.app.database.h \
	proxy/uk.org.thekelleys.dnsmasq.h \
	proxy/org.squid_cache.proxy.h

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) $(EXTRA_LDFLAGS)

proxy/%.xml:${CROOT}/usr/share/dbus-1/interfaces/%.xml
	cp $< $@

proxy/%.h:proxy/%.xml
	dbusxx-xml2cpp $< --proxy=$@

adaptor/%.h:adaptor/%.xml
	dbusxx-xml2cpp $< --adaptor=$@

%.o:%.cpp $(INC_FILES) $(GEN_FILES)
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(EXTRA_CXXFLAGS)

clean:
	rm -rf $(TARGET) $(OBJ_FILES) $(GEN_FILES)
